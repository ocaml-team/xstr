Source: xstr
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml,
 ocaml-findlib,
 dh-ocaml
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/ocaml-team/xstr.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/xstr
Homepage: http://projects.camlcity.org/projects/xstr.html

Package: libxstr-ocaml-dev
Architecture: any
Depends:
 ${ocaml:Depends},
 ocaml-findlib,
 ${shlibs:Depends},
 ${misc:Depends}
Provides:
 ${ocaml:Provides}
Description: OCaml library for frequent string operations
 xstr is an OCaml library for string operations.
 .
 This package implements frequent string operations: searching,
 replacing, splitting, matching. It is independent from the Str library,
 and can replace Str in many cases. Unlike Str, xstr is thread-safe.
 xstr does not implement regular expressions in general, but an
 important subset.  Some operations of xstr are performed as quickly as
 by Str; if the string to be processed is small, xstr is often faster
 than Str; if the string is big, xstr is up to half as fast than Str.
