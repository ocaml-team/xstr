xstr (0.2.1-25) unstable; urgency=medium

  * Team upload
  * Fix compilation with OCaml 5.2.0 (Closes: #1073914)

 -- Stéphane Glondu <glondu@debian.org>  Fri, 09 Aug 2024 09:20:55 +0200

xstr (0.2.1-24) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Depend on ocaml instead of transitional ocaml-nox
  * Add Rules-Requires-Root: no
  * Bump Standards-Version to 4.6.2

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 13.
  * Set debhelper-compat version in Build-Depends.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 02 Sep 2023 09:51:27 +0200

xstr (0.2.1-23) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Thu, 12 Sep 2019 10:29:06 +0200

xstr (0.2.1-22) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Switch debian/copyright to format 1.0
  * Update Vcs-*
  * Switch source package format to 3.0 (quilt)
  * Bump debhelper compat to 10
  * Bump Standards-Version to 4.0.1
  * Switch debian/rules to dh

  [ Stefano Zacchiroli ]
  * Remove myself from Uploaders

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from uploaders (Closes: #869320)

 -- Stéphane Glondu <glondu@debian.org>  Tue, 08 Aug 2017 17:54:59 +0200

xstr (0.2.1-21) unstable; urgency=low

  * Remove ./depend through debian/clean
  * Use dh-ocaml 0.9.1 features
  * Add debian/gbp.conf to use pristine-tar
  * Upgrade Standards-Version to 3.8.3 (section ocaml, README.source)

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 19 Dec 2009 12:49:49 +0100

xstr (0.2.1-20) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * upload to unstable
  * rebuild against OCaml 3.11, bump build-deps accordingly
  * debian/control:
    - bump standards version to 3.8.0 (no changes needed)
    - add missing ${misc:Depends} dependency
  * debian/patches/
    - build: delegate installd dir creation to makefile, rather instead
      of debhelper
  * bump debhelper compatibility level to 7
  * debian/rules: use ocaml.mk as a CDBS "rules" snippet

  [ Stephane Glondu ]
  * Update Homepage, fix watch file.
  * Switching packaging to git

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 26 Feb 2009 23:14:43 +0100

xstr (0.2.1-19) unstable; urgency=low

  * fix vcs-svn field to point just above the debian/ dir
  * add Homepage field to debian/control
  * update standards-version, no changes needed
  * set me as an uploader, d-o-m as the maintainer
  * debian/patches: rename 01_build.dpatch - (00list is enough for ordering)
  * debian/patches: add missing description to patch build.dpatch
  * move expansion of upstream comments to ocamldoc-like comments to a
    separate shell script, to avoid Makefile escaping issues

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 31 Dec 2007 18:09:32 +0100

xstr (0.2.1-18) unstable; urgency=low

  * debian/rules
    - enable generation of ocamldoc documentation (via CDBS)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 04 Sep 2007 17:54:16 +0200

xstr (0.2.1-17) experimental; urgency=low

  * rebuild with ocaml 3.10
  * debian/watch
    - add watch file
  * bump debhelper dep to 5 and add (previously missing) debian/compat

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 14 Jul 2007 11:34:22 +0200

xstr (0.2.1-16) unstable; urgency=low

  * debian/rules
    - use ocaml.mk
  * debian/control
    - bumped build dependency on ocaml-nox to >= 3.09.2-7, since we now use
      ocaml.mk

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  4 Nov 2006 09:35:21 +0100

xstr (0.2.1-15) unstable; urgency=low

  * debian/rules
    - removed no longer needed workaround for cdbs + dpatch
    - avoid to create debian/control from debian/control.in on ocamlinit
    - removed from the source package files which are generated at build time
      from the corresponding .in files
  * debian/control.in
    - file removed, no longer needed

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  6 Sep 2006 09:38:54 +0200

xstr (0.2.1-14) unstable; urgency=low

  * Upload to unstable.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 16 May 2006 20:12:53 +0000

xstr (0.2.1-13) experimental; urgency=low

  * Rebuilt against OCaml 3.09.2, bumped deps accordingly.
  * Bumped Standards-Version to 3.7.2 (no changes needed).

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 22:13:35 +0000

xstr (0.2.1-12) unstable; urgency=low

  * Rebuilt against OCaml 3.09.1, bumped deps accordingly.

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  7 Jan 2006 14:40:33 +0100

xstr (0.2.1-11) unstable; urgency=low

  * Rebuilt with ocaml 3.09
  * debian/control
    - bumped standards version
  * debian/*
    - use cdbs and dpatch
    - no longer hard coding of ocaml abi anywhere

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 12 Nov 2005 10:06:09 +0100

xstr (0.2.1-10) unstable; urgency=low

  * Rebuilt against ocaml 3.08.3

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 24 Mar 2005 22:48:27 +0100

xstr (0.2.1-9) unstable; urgency=low

  * rebuilt against ocaml 3.08.2

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 Dec 2004 16:34:30 +0100

xstr (0.2.1-8) unstable; urgency=low

  * rebuilt with ocaml 3.08
  * debian/control
    - bumped ocaml deps to 3.08
    - bumped standards-version to 3.6.1.1
    - changed ocaml deps to ocaml-nox

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Jul 2004 16:29:57 +0200

xstr (0.2.1-7) unstable; urgency=low

  * Changed section to libdevel

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  2 Oct 2003 19:26:55 +0200

xstr (0.2.1-6) unstable; urgency=low

  * Rebuilt with ocaml 3.07

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  1 Oct 2003 14:12:04 +0200

xstr (0.2.1-5) unstable; urgency=low

  * Rebuilt with ocaml 3.07beta2

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 22 Sep 2003 17:33:20 +0200

xstr (0.2.1-4) unstable; urgency=low

  * Removed Provides:.*-<version>

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 10 Mar 2003 13:52:37 +0100

xstr (0.2.1-3) unstable; urgency=low

  * Libdir transition to /usr/lib/ocaml/3.06
  * Changed depends and build depends to ocaml{,-base}-3.06-1
  * Reformatted upstream author in debian/copyright just to make lintian
    happy

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Dec 2002 13:02:24 +0100

xstr (0.2.1-2) unstable; urgency=low

  * Added 'Provides: libxstr-ocaml-dev-<version>'
  * Bumped Standards-Versions to 3.5.8
  * Better test on ocamlopt presence in debian/rules
  * Removed useless binary-indep target from debian/rules
  * Commented out some useless dh_* from debian/rules

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 28 Nov 2002 23:41:10 +0100

xstr (0.2.1-1) unstable; urgency=low

  * New upstream release
  * Rebuilt against ocaml 3.06 (Closes: Bug#158265)
  * Switched to debhelper 4
  * Removed mention of ocaml-xstr (ancient) from debian/control
  * Changed deps and build-deps to ocaml-3.06
  * Changed deps on ocaml-findlib from recommends to depends
  * Fixed some typos in CVS variables

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 27 Aug 2002 00:03:44 +0200

xstr (0.2-7) unstable; urgency=low

  * Renamed package to libxstr-ocaml-dev.
  * Some aesthetic changes in debian/rules.

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Feb 2002 09:24:16 +0100

xstr (0.2-6) unstable; urgency=low

  * Now build depends on ocaml >= 3.04-3, hopefully will compile also on
    ia64 and powerpc

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 11 Jan 2002 14:31:43 +0100

xstr (0.2-5) unstable; urgency=low

  * Rebuilt with ocaml 3.04

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 24 Dec 2001 09:20:32 +0100

xstr (0.2-4) unstable; urgency=low

  * Fixed spelling error in description (closes: Bug#125200).

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 18 Dec 2001 13:11:28 +0100

xstr (0.2-3) unstable; urgency=low

  * Rebuilt with ocaml 3.02

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 20 Aug 2001 21:33:58 +0200

xstr (0.2-2) unstable; urgency=low

  * Conditional build of "opt" target now check for /usr/bin/ocamlopt
    executableness (closes: Bug#96254).

 -- Stefano Zacchiroli <zack@debian.org>  Mon,  7 May 2001 13:13:38 +0200

xstr (0.2-1) unstable; urgency=low

  * Initial Release (closes: Bug#93417).

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  7 Apr 2001 01:01:51 +0200
